
"use strict";

use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
     "/libs/wcm/foundation/components/utils/Image.js",
     "/libs/sightly/js/3rd-party/q.js"], function(AuthoringUtils, Image, Q) {

    const properties = granite.resource.properties;

    var CONST = {
        LINK: "link",
        AUTHOR_IMAGE_CLASS: "cq-dd-image",
        PLACEHOLDER_SRC: "/etc/designs/default/0.gif"
    };

    var values = {};
    values.link = currentStyle[CONST.LINK] || properties[CONST.LINK];
         
    granite.resource.resolve(currentStyle.path + "/file").then(function (localImageResource) {
        values.imgSrc = currentStyle.path + "/file";
        return;
    }, function() {
    	// check if there's a local file image under the node
    	granite.resource.resolve(granite.resource.path + "/file").then(function (localImageResource) {
            values.imgSrc = granite.resource.path + "/file";
            return;
    	}, function() {
        	// The drag & drop image CSS class name
        	image.cssClass = CONST.AUTHOR_IMAGE_CLASS;
        
        	// Modifying the image object to set the placeholder if the content is missing
        	if (!image.fileReference()) {
            	values.imgSrc = CONST.PLACEHOLDER_SRC;
        	}
            return;
    	});

    });

    return values;
});
