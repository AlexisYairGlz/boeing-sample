"use strict"

use(function () {

    var values = {};

    const CONST = {
        IMG_SRC: "imgSrc",
        TEXT: "text",
        LINK: "link"
    }

    values.imgSrc = granite.resource.properties[CONST.IMG_SRC] || "spaceCapsule.jpg";
    values.text = granite.resource.properties[CONST.TEXT] || "DEFAULT TEXT. Configure component's text";
    values.link = granite.resource.properties[CONST.LINK] || "http://www.boeing.com/space/";

    return values;

});